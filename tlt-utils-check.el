  (require 'package)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
  (package-initialize)
(setq package-check-signature nil)

(setq tlt-utils-path "/home/vitus/Github/org-latex-utilities/")
(add-to-list 'load-path tlt-utils-path)

  (use-package tlt-utils
    :load-path tlt-utils-path
    :hook (TeX-mode . tlt-utils-mode)
    (org-mode . tlt-utils-mode))
