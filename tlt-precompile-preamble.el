(require 'tex-bar)

(defvar tlt-precompile-button-tb nil
  "Whether or not to add a precompilation button to the toolbar.")

(defcustom tlt-precompile-preamble t
  "Whether or not to use the functionality provided in `tlt-precompile-preamble'."
  :group 'tlt)

(defcustom tlt-precompile-show-precompilation nil
  "Whether or not to show the output buffer of the precompilation."
  :group 'tlt)

(defcustom tlt-precompile-preamble-name "preamble"
  "Name of preamble file, without extension."
  :group 'tlt)

(defcustom tlt-precompile-preamble-handout-extra "_handout"
  "Name for the preamble file used for handouts."
  :group 'tlt)

(defcustom tlt-precompile-latex-classes nil
  "List of cons pairs whose car is a string (the LaTeX class) and whose cdr is a string (the path to the configuration files).")

(defvar tlt-precompile-process nil
  "Stores the precompilation process.
  For internal use only.")

(defvar tlt-precompile-custom-preamble nil
  "Whether or not the `.org' file to be exported needs a custom preamble.
  For internal use only.")

(defvar tlt-precompile-marker nil
  "Whether or not in the process of precompiling a preamble.
  Used to avoid problems with `tlt-auxiliary-files-tex-add-advice-maybe';
  see `tlt-utils-unbox-aux-files-wrapper'.
  For internal use only.")

(defvar tlt-precompile-search-preamble-function #'tlt-precompile-search-preamble-raw-default
  "The name of the function used by default to search for a preamble.")

(defvar tlt-precompile-add-preamble-string nil
  "The preamble-string to be inserted by `tlt-precompile-preamble-template'
  at the beginning of an exported `.tex' file.
  For internal use only.")

(defun tlt-precompile-preamble-name-default ()
  (car (get 'tlt-precompile-preamble-name 'standard-value)))

(defun tlt-precompile-preamble-name-handout ()
  "Returns name for the handout preable without extension."
  (concat (tlt-precompile-preamble-name-default) tlt-precompile-preamble-handout-extra))

(defun tlt-precompile-preamble-string ()
  "Returns `tlt-precompile-preamble-name' plus '.tex' extension."
  (concat tlt-precompile-preamble-name ".tex"))

(defun tlt-precompile-preamble-string-handout ()
  "Returns `tlt-precompile-preamble-name' plus `tlt-precompile-preamble-handout-extra' plus '.tex' extension."
  (concat (tlt-precompile-preamble-name-handout) ".tex"))

(defun tlt-precompile-find-subdirectories (dir dirfiles)
  "Return a list of the full paths of the subdirectories of DIR in DIRFILES."
  (-non-nil (mapcar
             (lambda (x)
               (when (and (file-directory-p (expand-file-name x dir))
                          (not (member  x '("." ".."))))
                 (expand-file-name x dir)))
             dirfiles)))

(defun tlt-precompile-search-file-subfolders (dir name-base extension &rest args)
  "In the current buffer's directory and all of its subdirectories (one level, not recursive),
search for a file whose name is 'NAME-BASE.EXTENSION'.
If such a file is found, return its full path.
If said file is not found, return nil."
  (let* ((base (concat name-base extension))
         (dirfiles (directory-files dir))
         (subdirs (tlt-precompile-find-subdirectories dir dirfiles))
         (dirs (push dir subdirs))) ; dir appended so that if two preambles exist, the one in the current folder is selected

    (let ((value))
      (dolist (element dirs value)
        (when (file-exists-p (expand-file-name base element))
          (setq value element)))

      (when value (expand-file-name base value)))))

(defun tlt-precompile-search-preamble-raw (dir name)
  "Search for the file NAME.tex in DIR.
If not found, return `nil'."
  (tlt-precompile-search-file-subfolders dir name ".tex"))

(defun tlt-precompile-search-preamble-precompiled (dir name)
  "Search for the file NAME.fmt in DIR and return its path.
If not found, return `nil'."
  (tlt-precompile-search-file-subfolders dir name ".fmt"))

(defun tlt-precompile-search-preamble-raw-default (dir &rest args)
  "Search for a .tex file in DIR whose base-name
is the output of `tlt-precompile-preamble-name-default'.
If not found, return `nil'."
  (tlt-precompile-search-preamble-raw dir (tlt-precompile-preamble-name-default)))

(defun tlt-precompile-search-preamble-precompiled-default (dir &rest args)
  "Search for an .fmt file in DIR whose base-name
is the output of `tlt-precompile-preamble-name-default'.
If not found, return `nil'."
  (tlt-precompile-search-preamble-precompiled
   dir
   (tlt-precompile-preamble-name-default)))

(defun tlt-precompile-search-preamble-raw-handout (dir &rest args)
  "Search for a .tex file in DIR whose base-name
is the output of `tlt-precompile-preamble-name-handout'.
If not found, return `nil'."
  (tlt-precompile-search-preamble-raw dir (tlt-precompile-preamble-name-handout)))

(defun tlt-precompile-search-preamble-precompiled-handout (dir &rest args)
  "Search for an .fmt file in DIR whose base-name
is the output of `tlt-precompile-preamble-name-handout'.
If not found, return `nil'."
  (tlt-precompile-search-preamble-precompiled dir (tlt-precompile-preamble-name-handout)))

(defcustom tlt-precompile-engine (replace-regexp-in-string
                                  "tex"
                                  "latex"
                                  (prin1-to-string TeX-engine))
  "Engine to run precompiling command with.")

(defvar tlt-precompile-command
  "%s -ini -interaction nonstopmode -shell-escape -jobname=\"%s\" \"&%s '%s' \\dump\""
  "Command to be run when precompiling a preamble.
The wildcards are placeholders for the TeX-engine,
the preamble name and the TeX-engine once again.")

(defvar tlt-precompile-list-item `("Precompiling" ,tlt-precompile-command tlt-precompile-run-precompile nil
                                   (plain-tex-mode latex-mode doctex-mode org-mode)
                                   :help "Precompile the preamble at hand for faster compiling.")
  "List item needed to run the precompilation command.")

(defun tlt-precompile-command (&optional engine name)
  "Return the LaTeX precompilation command based on the variable `tlt-precompile-command'.
The LaTeX ENGINE and the preamble NAME can be set optionally.
If they are not, the values of `tlt-precompile-engine'
 and `tlt-precompile-preamble-name' are used, respectively."
  (let ((engine (or engine tlt-precompile-engine))
        (name (or name tlt-precompile-preamble-name)))
    (format tlt-precompile-command
            engine name engine "%b")))

(defun tlt-precompile-preamble (preamble-path &optional name)
  "Precompile the preamble at PREAMBLE-PATH.
If optional argument NAME is provided, set the base
of the preamble to NAME.fmt.
Signal an error if PREAMBLE-PATH is `nil'."
  (if (null preamble-path)
      (error "Could not find any preamble file")
    (cl-letf* ((TeX-command-list `(("Precompiling" ,(tlt-precompile-command nil name) tlt-precompile-run-precompile nil
                                    (plain-tex-mode latex-mode doctex-mode org-mode)
                                    :help "Precompile the preamble at hand for faster compiling.")))

               (TeX-expand-list-builtin  ; replace file name function
                (add-to-list 'TeX-expand-list-builtin
                             '("%b" (lambda (&optional one two) preamble-path) nil t)))   ; prepending to list works; old entry further down is ignored
               (TeX-show-compilation tlt-precompile-show-precompilation) ; own variable whether to show output directly
               (TeX-process-asynchronous nil)   ; make the process asynchronous (needed!)
               (TeX-command-sequence-max-runs-same-command 1) ; only run once
               (binding (substitute-command-keys; store the command bindings
                         "\\<tlt-precompile-utils-map>\\[tlt-precompile-switch-to-precompilation-output]"))    ; of the buffer switch command
               ((symbol-function 'substitute-command-keys)
                (lambda (string &optional noface) binding))   ; and return it when `substitute-command-keys' is called
               ((symbol-function 'TeX-master-directory)
                (lambda () (file-name-directory preamble-path))))
                                        ; store the process returned to make it accessible later on
      (setq tlt-precompile-marker t)
      (setq tlt-precompile-process
            (TeX-command "Precompiling" (lambda () preamble-path)))
      (setq tlt-precompile-marker nil))))   ; wait for the process to finish

(defun tlt-precompile-preamble-default ()
  "Precompile the `preamble.tex' file corresponding to the current buffer's `.org' file.
see `tex-file-get-create'."
  (interactive)
  (tlt-precompile-preamble (tlt-precompile-search-preamble-raw-default (file-name-directory (buffer-file-name)))))

(defun tlt-precompile-preamble-handout ()
  "Search for a preamble for handouts based on the handout preamble file and precompile it.
If such a handout preamble does not yet exist, create it based off the default preamble file.
The handout-preamble's file name is according to `tlt-precompile-preamble-string-handout'.
Also compare with `tlt-precompile-command'."
  (interactive)
  (let* ((dir (file-name-directory (buffer-file-name)))
         (tlt-precompile-command
          (format "%s -ini -interaction nonstopmode -shell-escape -jobname=\"%s\" \"&%s '%s' \\dump\""
                  tlt-precompile-engine (tlt-precompile-preamble-name-handout) tlt-precompile-engine "%b"))

         (TeX-command-list `(("Precompiling" ,tlt-precompile-command tlt-precompile-run-precompile nil
                              (plain-tex-mode latex-mode doctex-mode org-mode)
                              :help "Precompile the preamble at hand for faster compiling."))))

    (unless (file-exists-p (tlt-preamble-handout-path dir))   ; If the handout-preamble does not exist yet,
      (tlt-preamble-handout-file))   ; create it

    (tlt-precompile-preamble (tlt-precompile-search-preamble-raw-handout dir))))

(defun tlt-precompile-handout-preamble-p ()
  "Return non-`nil' if the value of `tlt-precompile-preamble-name' has changed
to the value of `tlt-precompile-preamble-name-handout', and else otherwise."
  (equal tlt-precompile-preamble-name (tlt-precompile-preamble-name-handout)))

(defun tlt-precompile-ensure-preamble-path (dir &optional name main-dir)
  "Search in DIR and all of its subdirectories for a
  precompiled preamble whose base-name is NAME.
  Move the precompiled preamble to DIR, or, if
  optional parameter MAIN-DIR is provided, copy it to MAIN-DIR.
  Signal an error if no preamble was found."
  (let* ((name (or name tlt-precompile-preamble-name))
         (preamble-file (or (tlt-precompile-search-preamble-precompiled dir name)
                            (error "No preamble found")))
         (preamble-dir (file-name-directory preamble-file))
         (preamble-base (file-name-nondirectory preamble-file))
         (main-dir-file (if main-dir
                            (expand-file-name preamble-base main-dir)
                          regexp-unmatchable)))
    ;; If MAIN-DIR is provided and the preamble does not exist there, copy it ;;
    (if (and main-dir (not (file-exists-p main-dir-file)))
        (copy-file preamble-file main-dir-file)
      ;; else, if the preamble does not exist in DIR, move it there ;;
      (unless (file-exists-p (expand-file-name preamble-base dir))
        (rename-file preamble-file (expand-file-name preamble-base dir))))))

(defun tlt-precompile-ensure-preamble (dir &optional name main-dir file &rest args)
  "Search for a precompiled preamble in DIR and all of its subdirectories.
  If there is none, search for an unprecompiled preamble the same way and precompile it.
  Move that preamble to DIR or, if provided as an optional argument, to MAIN-DIR.
  If there is neither a precompiled nor an unprecompiled preamble, return an error."
  (let ((org-file (or file buffer-file-name))
        (name (or name tlt-precompile-preamble-name))) ; to save current buffer

    (if (tlt-precompile-handout-preamble-p)

        ;; handout-preamble ;;

        (progn
          (let ((pre (tlt-precompile-search-preamble-precompiled-handout dir))  ; precompiled is preferred, nil if non-existent
                (raw (tlt-precompile-search-preamble-raw-handout dir))
                (raw-default (tlt-precompile-search-preamble-raw-default dir)))

            (cond
             (raw (tlt-precompile-preamble-handout))     ; if there is a raw handout preamble, precompile it
             (raw-default ; if there only is a raw default preamble
              (tlt-preamble-handout-file)   ; make a raw handout preamble
              (tlt-precompile-preamble-handout))  ; and precompile it

             (t (error "No preamble found - neither precompiled nor as .tex file"))))) ; if there is nothing suitable, return an error

      ;; default-preamble ;;

      (let* ((pre (tlt-precompile-search-preamble-precompiled dir name)) ; precompiled is preferred, nil if non-existent
             (raw (tlt-precompile-search-preamble-raw dir name)))

        (unless pre
          (cond
           (raw (tlt-precompile-preamble raw name))     ; if there is a raw default preamble, precompile it
           (t (error "No preamble found - neither precompiled nor as .tex file"))))))   ; if there is nothing suitable, return an error
    ;; possibly moving the preamble ;;

    (tlt-precompile-ensure-preamble-path dir name main-dir)
    (find-file org-file)))

(defun tlt-precompile-switch-to-precompilation-output ()
  "Display the buffer containing the output from the precompilation.
Inform where the precompilation has succeded."
  (interactive)
  (let* ((buf (process-buffer tlt-precompile-process))
         (bufname (buffer-name buf))
         (strlist (split-string bufname "/" t nil))
         (names (last strlist 2))
         (filename (nth 1 names))
         (filename-finish (string-trim filename nil " output\\*"))
         (dirname (concat (nth 0 names) "/")))
    (prog1
        (message (format "Output from precompilation on %s in %s" filename-finish dirname))
      (display-buffer buf))))

(defun tlt-preamble-handout-path (dir &rest args)
  "Return the path of a (potentially not yet existent) handout preamble."
  (let* ((preamble-path (tlt-precompile-search-preamble-raw-default dir))
         (preamble-folder (file-name-directory preamble-path))
         (tlt-preamble-handout-path (expand-file-name (tlt-precompile-preamble-string-handout) preamble-folder)))
    tlt-preamble-handout-path))

(defun tlt-preamble-handout-file ()
  "Create a preamble file named after `tlt-precompile-preamble-string-handout'
with the same contents as the usual preamble file,
but add the optional parameter 'handout' to the '\\documentclass' command."
  (interactive)
  (let ((dir (file-name-directory (buffer-file-name)))
        (preamble (tlt-precompile-search-preamble-raw-default dir))
        (preamble-handout (tlt-preamble-handout-path dir)))
    (with-temp-buffer
      (insert-file-contents preamble)
      (goto-char (point-min))
      (re-search-forward "^\\\\documentclass\\[") ; go to the document class definition
      (insert "handout,")    ; and add "handout" as an optional argument
      (write-file preamble-handout))))     ; write that file

(defun tlt-toggle-off-notes ()
  "Toggle off any line beginning with
'#+LATEX_HEADER:\\setbeameroption{show notes.'
Includes these prominent cases (DIR being any direction):
'#+LATEX_HEADER:\\setbeameroption{show notes}'
'#+LATEX_HEADER:\\setbeameroption{show notes on second screen=DIR}'."
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "^\\#\s\\#\\+LATEX_HEADER:\\\\setbeameroption{show notes" nil t)  ; ^ to not double comment
      (beginning-of-line)
      (insert "# ")
      (end-of-line))))

(defun tlt-beamer-handout ()
  "Make a beamer handout using a precompiled preamble.
Temporarily comment out the note-part of the pdf;
see `tlt-toggle-off-notes'."
  (interactive)
  (let* ((buffer buffer-file-name)
         (dir (file-name-directory buffer))
         (tlt-precompile-preamble-name (tlt-precompile-preamble-name-handout)) ; needed for `tlt-precompile-preamble-template'
         (tlt-precompile-preamble-string (concat tlt-precompile-preamble-name ".fmt"))
         (file (org-export-output-file-name ".tex" nil)))

    (tlt-precompile-ensure-preamble dir)    ; make sure preamble exists at the right place
    (find-file buffer)
    (let ((tlt-precompile-search-preamble-function #'tlt-precompile-search-preamble-precompiled-handout) ; needed for `tlt-precompile-preamble-template'))
          (contents (org-export-as 'beamer)))

      (tlt-toggle-off-notes)

      (with-temp-buffer
        (insert contents)
        (write-file file))

      (org-open-file
       (org-latex-compile file))
      (revert-buffer t t)))) ; to revert toggling off the notes part

(defun tlt-precompile-preamble-sentinel (_process _name)
  "Clean up TeX output buffer after precompiling a preamble."
  (setq tlt-precompile-process _process)
  (goto-char (point-max))
  (let ((binding (substitute-command-keys  ;  store the command bindings
                  "\\<tlt-precompile-utils-map>\\[tlt-precompile-switch-to-precompilation-ouput]"))) ;  of the buffer switch command)
    (cl-letf (((symbol-function 'substitute-command-keys) (lambda (string &optional noface) binding)))

      (cond

       ((re-search-backward "exited abnormally\\|^Emergency stop\\|! I can't find file" nil t)
        (message (concat "TeX exited abnormally!"
                         "Type `%s' to display output.")
                 (substitute-command-keys
                  "\\<TeX-mode-map>\\[TeX-recenter-output-buffer]")))

       ((re-search-backward "! Can't \dump a format with native fonts or font-mappings." nil t)
        (message "Errors while recompiling! XeLaTeX can't handle native fonts. Remove fontspec and packages that depend on it from the preamble and recompile."))

       ((re-search-backward "^INFO - \\(WARNINGS\\|ERRORS\\): \\([0-9]+\\)" nil t)
        (message (concat "Precompilation finished with %s %s. "
                         "Type `%s' to display output.")
                 (match-string 2) (downcase (match-string 1))
                 (substitute-command-keys
                  "\\<TeX-mode-map>\\[TeX-recenter-output-buffer]"))
        (setq TeX-command-next TeX-command-default))

       ((re-search-backward "^FATAL" nil t)
        (message (concat "There was a fatal error during precompiling!"
                         "Type `%s' to display output.")
                 (substitute-command-keys
                  "\\<TeX-mode-map>\\[TeX-recenter-output-buffer]"))
        (setq TeX-command-next "Precompiling"))

       (t
        (message "Precompiling finished successfully! You can run TeX as usual now.")
        (setq TeX-command-next TeX-command-default))))))

(defun tlt-precompile-run-precompile (name command file)
  "Create a process for NAME using COMMAND to precompile FILE.
Return that process."
  (let ((process (TeX-run-command name command file)))
    (setq TeX-sentinel-function #'tlt-precompile-preamble-sentinel)
    (if TeX-process-asynchronous
        process
      (TeX-synchronous-sentinel name file process))))

(defun tlt-precompile-directory-wrapper (fun name command file)
  "Wrapper for `TeX-command-run' to change the process buffer's default directory."
  ;; If we are in the middle of a precompiling command, ;;
  (let ((dir (file-name-directory file)))
    (if (equal (car TeX-expand-list-builtin) `("%b" tlt-precompile-search-preamble-function nil t)) ; here is the problem!

        ;; let `TeX-master-directory' return the preamble's path

        (flet ((TeX-master-directory () (file-name-directory (funcall tlt-precompile-search-preamble-function dir))))
          (funcall fun name command file))

      ;; else just call `TeX-master-directory' unaltered

      (funcall fun name command file))))

(defun tlt-precompile-tool-bar-toggle-tlt-precompile-button ()
  "Add/remove precompilation button to/from the tool-bar in TeX mode."
  (let ((button (assoc 'precompile TeX-bar-TeX-button-alist)))
                                        ; if the precompiling command is in the tool-bar, remove it
    (if button
        (progn

          (setq TeX-bar-TeX-button-alist (delete tlt-precompile-button TeX-bar-TeX-button-alist))
          (setq TeX-bar-TeX-buttons (delete 'precompile TeX-bar-TeX-buttons))
          (setq TeX-bar-LaTeX-buttons (delete 'precompile TeX-bar-LaTeX-buttons))
          (LaTeX-install-toolbar)
          (TeX-install-toolbar))

                                        ; if it is not, add it
      (when tlt-precompile-button-tb
        (add-to-list 'TeX-bar-TeX-button-alist tlt-precompile-button t)) ; t for appending
      (add-to-list 'TeX-bar-TeX-buttons 'precompile t)
      (add-to-list 'TeX-bar-LaTeX-buttons 'precompile t)
      (LaTeX-install-toolbar)
      (TeX-install-toolbar)))

  nil)

(defun tlt-precompile-org-latex-sections (config-class)
  `(,config-class
    "[NO-DEFAULT-PACKAGES]\n[PACKAGES]\n[EXTRA]" ; no header-string
    ("\\section{%s}" . "\\section*{%s}")
    ("\\subsection{%s}" . "\\subsection*{%s}")
    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))

(defun tlt-precompile-org-beamer-sections (config-class)
  `(,config-class
    "[NO-DEFAULT-PACKAGES]\n[PACKAGES]\n[EXTRA]" ; no header-string
    ("\\section{%s}" . "\\section*{%s}")
    ("\\subsection{%s}" . "\\subsection*{%s}")
    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))

(defun tlt-precompile-org-beispiel-sections ()
  `("beispiel"
    "[NO-DEFAULT-PACKAGES]\n[PACKAGES]\n[EXTRA]" ; no header-string
    ("\\section{%s}" . "\\section*{%s}")
    ("\\subsection{%s}" . "\\subsection*{%s}")
    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))

(defun org-latex-ensure-config-class (config-class backend)
  "If the CONFIG-CLASS does not have an apprioriate entry in `org-latex-classes',
create it by using either a function of the form `tlt-precompile-org-CONFIG-CLASS-sections'
or a default depending on BACKEND.
If the former, the custom function needs to be defined without an argument.
Return the CONFIG-CLASS if it was added, and `nil' otherwise."
  (unless (assoc-string config-class org-latex-classes)
    (let ((section-fun (concat "tlt-precompile-org-" config-class "-sections")))
      ;; If a variable specifying the sections for CONFIG-CLASS exists,
      ;; add its value to `org-latex-classes'.
      (if (functionp (intern section-fun))
          (push (funcall (intern section-fun))
                org-latex-classes)
        ;; else, add the standard sections defined in
        ;; `tlt-precompile-org-beamer-sections' and
        ;; `tlt-precompile-org-latex-sections', respectively
        (add-to-list 'org-latex-classes (cond ((equal backend 'beamer)
                                               (funcall #'tlt-precompile-org-beamer-sections config-class))

                                              ((equal backend 'latex)
                                               (funcall #'tlt-precompile-org-latex-sections config-class))

                                              (t (error (format "Backend %s not beamer or latex" backend)))))))))

(defun tlt-precompile-ensure-preamble-advice (backend &rest args)
  "If BACKEND is 'latex or 'beamer and the LaTeX class of the `.org' file occurs
        in `tlt-precompile-latex-classes', ensure that the precompiled preamble
        for BACKEND, which is specified there, is in the current org file's path.
        If the latex-class is 'custom', ensure a preamble from one of the
        `.org' file's subdirectories.
        Advice for `org-export-as'."
  (when (and buffer-file-name                  ; we are in a file buffer
             (not TeX-command-next)            ; this is not an intermediary latex command
             (or (equal backend 'latex)        ; and it exports to to plain latex
                 (equal backend 'beamer)))     ; or to latex beamer

    (let* ((file buffer-file-name)
           (dir (file-name-directory file))
           (class (or (plist-get (org-export--get-inbuffer-options 'latex) :latex-class)
                      (if (equal backend 'beamer) "beamer")
                      org-latex-default-class)) ; default-class if not provided
           (config-dir (nth 1 (assoc-string class tlt-precompile-latex-classes)))) ; nil if no entry

      ;; ensure the entry for CLASS in `org-latex-classes'
      (org-latex-ensure-config-class class backend)

      (cond
       ;;  If we export to latex and the CLASS is custom,
       ;;  ensure the custom preamble in one of the subdirectories
       ((equal class "custom")
        (setq tlt-precompile-custom-preamble t) ; to communicate with template-function
        (tlt-precompile-ensure-preamble dir tlt-precompile-preamble-name))
       ;;  If we export to latex and the class is in `tlt-precompile-latex-classes',
       ;;  ensure that the preamble specified there is in the `.org' file's path
       ((and (stringp config-dir)
             (file-directory-p config-dir))
        (setq tlt-precompile-add-preamble-string (concat tlt-precompile-preamble-name "_" class))
        (let ((tlt-precompile-preamble-name tlt-precompile-add-preamble-string)) ; necessary
          (tlt-precompile-ensure-preamble config-dir
                                          tlt-precompile-preamble-name
                                          dir
                                          file)))
       (t nil))))) ; else return nil

(defun tlt-precompile-preamble-template (fun contents info)
  "If `tlt-precompile-add-preamble-string' is non-`nil',
add the string specified by `tlt-precompile-preamble-name'
at the very beginning of the file."
  ;; if a string should be added to the template
  ;; (see `tlt-precompile-ensure-preamble-advice')
  ;; delete the \documentclass definition and
  ;; add the preamble-string at the very beginning
  (cond

   (tlt-precompile-custom-preamble
      (prog1
          (let ((template (funcall fun contents info)))
            (concat (format "%s%s\n" "%&" tlt-precompile-preamble-name)                                   ; add the preamble string
                    (replace-regexp-in-string  "\\\\documentclass\\[[^]]+]{[a-z 0-9]+}\n" "" template)))  ; delete \documentclass
        (setq tlt-precompile-add-preamble-string nil)                                                    ; reset `tlt-precompile-add-preamble-string'
        (setq tlt-precompile-custom-preamble nil)))

    (tlt-precompile-add-preamble-string
        (prog1
            (let ((template (funcall fun contents info)))
              (concat (format "%s%s\n" "%&" tlt-precompile-add-preamble-string)                                   ; add the preamble string
                      (replace-regexp-in-string  "\\\\documentclass\\[[^]]+]{[a-z 0-9]+}\n" "" template)))  ; delete \documentclass
          (setq tlt-precompile-add-preamble-string nil)))                                                    ; reset `tlt-precompile-add-preamble-string'

    (t (funcall fun contents info))))

(defun tlt-precompile-add-advice-maybe ()
  "Make all changes necessary for `tlt-precompile-preamble' to work in `tlt-utils-mode'."
  (when tlt-precompile-preamble
    (advice-add #'org-export-as       :before #'tlt-precompile-ensure-preamble-advice)
    (advice-add #'TeX-run-command     :around #'tlt-precompile-directory-wrapper)
    (advice-add #'org-beamer-template :around #'tlt-precompile-preamble-template)
    (advice-add #'org-latex-template  :around #'tlt-precompile-preamble-template)

    (add-hook 'tool-bar-mode-hook #'tlt-precompile-tool-bar-toggle-tlt-precompile-button)
    (add-to-list 'TeX-command-list tlt-precompile-list-item t)))

(defun tlt-precompile-remove-advice ()
  "Remove all the changes made by `tlt-precompile-add-advice-maybe'."
  (advice-remove #'org-export-as #'tlt-precompile-ensure-preamble-advice)
  (advice-remove #'TeX-run-command #'tlt-precompile-directory-wrapper)
  (advice-remove #'org-beamer-template #'tlt-precompile-preamble-template)
  (advice-remove #'org-latex-template  #'tlt-precompile-preamble-template)

  (remove-hook 'tool-bar-mode-hook #'tlt-precompile-tool-bar-toggle-tlt-precompile-button)
  (setq TeX-command-list (delete tlt-precompile-list-item TeX-command-list)))

(provide 'tlt-precompile-preamble)

;; tlt-precompile-preamble.el ends here ;;
